package com.example.calc.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calc.enums.ActionType;
import com.example.calc.CalcOperations;
import com.example.calc.enums.OperationType;
import com.example.calc.R;
import com.example.calc.enums.Symbol;
import com.example.calc.exceptions.ZeroDivizionException;

import java.util.EnumMap;

public class MainActivity extends AppCompatActivity {


    private Button btnPlus;
    private Button btnMinus;
    private Button btnMul;
    private Button btnDiv;
   // private Button btnCalc;
 //   private Button btnClear;

    private TextView textResult;

    private EnumMap<Symbol, Object> commands = new EnumMap<Symbol, Object>(Symbol.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeComponents();
    }


    public void initializeComponents(){

        textResult = (TextView) findViewById(R.id.result);

        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnMul = (Button) findViewById(R.id.btnMul);
        btnDiv = (Button) findViewById(R.id.btnDiv);
      // btnCalc = (Button) findViewById(R.id.btnCalc);
       // btnClear = (Button) findViewById(R.id.btnClear);

        btnPlus.setTag(OperationType.PLUS);
        btnMinus.setTag(OperationType.MINUS);
        btnDiv.setTag(OperationType.DIVIDE);
        btnMul.setTag(OperationType.MULTIPLY);
    }

    private OperationType operationType;
    private ActionType lastAction;

    public void buttonClick(View view){



        switch(view.getId()){
            case R.id.btnPlus:
            case R.id.btnMinus:
            case R.id.btnMul:
            case R.id.btnDiv: {


                operationType = (OperationType) view.getTag();

                if(lastAction == ActionType.OPERATION){
                    commands.put(Symbol.OPERATION, operationType);
                    return;
                }

                if(!commands.containsKey(Symbol.OPERATION)){
                    if(!commands.containsKey(Symbol.FIRST_DIGIT)){
                        commands.put(Symbol.FIRST_DIGIT, textResult.getText());
                    }
                    commands.put(Symbol.OPERATION, operationType);
                } else if(!commands.containsKey(Symbol.SECOND_DIGIT)){
                    commands.put(Symbol.SECOND_DIGIT, textResult.getText());
                    doCalc();
                    commands.put(Symbol.OPERATION, operationType);
                    commands.remove(Symbol.SECOND_DIGIT);
                }

                lastAction = ActionType.OPERATION;
                break;
            }
            case R.id.btnClear:{
                textResult.setText("0");
                commands.clear();
                lastAction = ActionType.CLEAR;
                break;
            }

            case R.id.btnCalc:{

                if(lastAction == ActionType.OPERATION){
                    return;
                }
                if(commands.containsKey(Symbol.FIRST_DIGIT)&& commands.containsKey(Symbol.OPERATION)) {
                    commands.put(Symbol.SECOND_DIGIT, textResult.getText());
                    doCalc();
                    commands.clear();

            }
                lastAction = ActionType.CALCULATION;
                break;
            }

            case R.id.btnPoint:{
                if(commands.containsKey(Symbol.FIRST_DIGIT) && getDouble(textResult.getText().toString())== getDouble(commands.get(Symbol.FIRST_DIGIT).toString())){
                    textResult.setText("0"+view.getContentDescription().toString());
                }
                if(!textResult.getText().toString().contains(",")){
                    textResult.setText(textResult.getText()+",");
                }
                lastAction = ActionType.COMMA;
                break;
            }
            default:
                if(textResult.getText().toString().equals("0") || (commands.containsKey(Symbol.FIRST_DIGIT) && getDouble(textResult.getText()) ==
                        getDouble(commands.get(Symbol.FIRST_DIGIT)) || lastAction == ActionType.CALCULATION)){

                    textResult.setText(view.getContentDescription().toString());

                }else{
                    textResult.setText(textResult.getText() + view.getContentDescription().toString());
                }
                lastAction = ActionType.DIGIT;
        }
    }

    private void showToastMessage(int messageId){
        Toast message = Toast.makeText(this, messageId, Toast.LENGTH_SHORT);
        message.setGravity(Gravity.TOP, 0, 300);
        message.show();
    }

    public void doCalc(){
        OperationType operationType2  = (OperationType) commands.get(Symbol.OPERATION);

        double result = 0;

        try {
            result = calc(operationType2, getDouble(commands.get(Symbol.FIRST_DIGIT)), getDouble(commands.get(Symbol.SECOND_DIGIT)));
        }catch(ZeroDivizionException zd){showToastMessage(R.string.zero_division_error); return;}

        if(result % 1 == 0){
            textResult.setText(String.valueOf((int)result));
        }else {
            textResult.setText(String.valueOf(result));
        }
        commands.put(Symbol.FIRST_DIGIT, result);
    }

    private Double calc(OperationType operationType, double a, double b){

        switch(operationType){
            case PLUS: return CalcOperations.add(a,b);
            case MINUS: return CalcOperations.substract(a,b);
            case DIVIDE: return CalcOperations.divide(a,b);
            case MULTIPLY: return CalcOperations.multiply(a,b);
        }
        return null;
    }

    private double getDouble(Object value){
        double result;

        try{
            result = Double.valueOf(value.toString().replace(',','.')).doubleValue();
        } catch(Exception e){
            e.printStackTrace();
            result = 0;
        }
        return result;
    }
}
