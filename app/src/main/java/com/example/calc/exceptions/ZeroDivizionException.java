package com.example.calc.exceptions;

/**
 * Created by Александр on 19.06.2016.
 */
public class ZeroDivizionException extends ArithmeticException {
    public ZeroDivizionException(){}

    public ZeroDivizionException(String msg){
        super(msg);
    }

}
