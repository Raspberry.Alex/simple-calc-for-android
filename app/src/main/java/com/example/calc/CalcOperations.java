package com.example.calc;

import com.example.calc.exceptions.ZeroDivizionException;

/**
 * Created by Александр on 16.06.2016.
 */
public class CalcOperations {

    public static double add(double a, double b){
        return a+b;
    }

    public static double substract(double a, double b){
        return a-b;
    }

    public static double divide(double a, double b){
        if(b==0){
            throw new ZeroDivizionException();
        }
        return a/b;
    }

    public static double multiply(double a, double b){
        return a*b;
    }


}
