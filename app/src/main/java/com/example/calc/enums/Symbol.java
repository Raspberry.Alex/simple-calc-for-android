package com.example.calc.enums;

/**
 * Created by Александр on 16.06.2016.
 */
public enum Symbol {

    FIRST_DIGIT,
    OPERATION,
    SECOND_DIGIT
}
