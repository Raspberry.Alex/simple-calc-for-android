package com.example.calc.enums;

/**
 * Created by Александр on 19.06.2016.
 */
public enum ActionType {
    OPERATION,
    CALCULATION,
    CLEAR,
    DIGIT,
    COMMA,
    DELETE
}
